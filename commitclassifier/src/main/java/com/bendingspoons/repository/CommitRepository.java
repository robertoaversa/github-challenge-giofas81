package com.bendingspoons.repository;

import com.bendingspoons.domains.Commit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommitRepository extends CrudRepository<Commit,String> {}
