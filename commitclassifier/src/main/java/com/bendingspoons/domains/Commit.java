package com.bendingspoons.domains;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity()
@Table(name = "commits")
public class Commit {
    @Id
    @Column
    private String sha;

    @Column
    private String date;

    @Column
    String author;

    @Column
    String message;

    @Column
    Boolean external = false;


    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean isExternal() {
        return external;
    }

    public void setExternal(Boolean external) {
        this.external = external;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("sha", sha)
                .append("date", date)
                .append("author", author)
                .append("message", message)
                .append("external", external)
                .toString();
    }

}
