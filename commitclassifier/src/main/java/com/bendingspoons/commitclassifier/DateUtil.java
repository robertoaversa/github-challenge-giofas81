package com.bendingspoons.commitclassifier;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtil {
    private static final String TIME_FORMATS = "dd MMM yyyy, HH:mm zzz";
    public static final Locale DEFAULT_LOCALE = Locale.forLanguageTag("it-IT");

    public static String toLocalDate(Date commitDate) {
        SimpleDateFormat df = new SimpleDateFormat(TIME_FORMATS, DEFAULT_LOCALE);
        df.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        return df.format(new Date(commitDate.getTime()));
    }

    public static Instant fromLocalDate(LocalDate localDate){
        return localDate.atStartOfDay().toInstant(ZoneOffset.UTC);
    }
}
