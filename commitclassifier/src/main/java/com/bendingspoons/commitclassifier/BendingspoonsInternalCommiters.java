package com.bendingspoons.commitclassifier;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashSet;

import static java.util.Optional.ofNullable;

@Configuration
public class BendingspoonsInternalCommiters {
    private HashSet<String> bendingspoonsCommiters;

    @Autowired
    private ResourceLoader resourceLoader;

    @PostConstruct
    public void load() throws IOException {
        HashSet<String> bendingspoonsCommiters = new HashSet<>();
        Resource fileResource = resourceLoader.getResource("classpath:members.json");
        String jsonBendingspoonsCommiters = new String(Files.readAllBytes(fileResource.getFile().toPath()));

        JsonParser parser = new JsonParser();
        JsonArray array = parser.parse(jsonBendingspoonsCommiters).getAsJsonArray();
        for (JsonElement jsonObject : array) {
            JsonObject user = jsonObject.getAsJsonObject();
            bendingspoonsCommiters.add(user.get("login").getAsString());
        }
        this.bendingspoonsCommiters = bendingspoonsCommiters;
    }

    public Boolean isThisExternal(String userName) {
        return ofNullable(userName).
                map(login -> !bendingspoonsCommiters.contains(login))
                .orElse(false);
    }

}
