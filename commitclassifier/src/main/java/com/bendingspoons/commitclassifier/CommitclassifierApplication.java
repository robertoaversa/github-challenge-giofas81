package com.bendingspoons.commitclassifier;

import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GitHub;
import org.kohsuke.github.GitHubBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.io.IOException;
import java.time.LocalDate;

import static org.kohsuke.github.RateLimitHandler.WAIT;

@SpringBootApplication
@EntityScan(basePackages = {"com.bendingspoons.domains"})
@EnableJpaRepositories("com.bendingspoons.repository")
@ComponentScan(basePackages = {"com.bendingspoons.commitclassifier"})
public class CommitclassifierApplication implements CommandLineRunner {

    private final static String REPO = "BendingSpoons/katana-swift";
    private static final LocalDate DATE_27_6_2018 = LocalDate.of(2018, 6, 27);

    @Autowired
    CommitClassifier commitClassifier;

    public static void main(String[] args) {
        SpringApplication.run(CommitclassifierApplication.class, args);

    }

    @Override
    public void run(String... args) throws Exception {
        commitClassifier.until(DATE_27_6_2018);
    }


    @Bean
    public GitHub gitHub() throws IOException {
        //Block until the API rate limit is reset. Useful for long-running batch processing.
        return GitHubBuilder.fromCredentials().withRateLimitHandler(WAIT).build();
    }

    @Bean
    public GHRepository katanaRepo(GitHub gitHub) throws IOException {
        return gitHub.getRepository(REPO);
    }


}
