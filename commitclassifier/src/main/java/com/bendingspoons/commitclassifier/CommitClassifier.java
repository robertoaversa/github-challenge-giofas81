package com.bendingspoons.commitclassifier;

import com.bendingspoons.repository.CommitRepository;
import com.bendingspoons.domains.Commit;
import org.kohsuke.github.GHCommit;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GHUser;
import org.kohsuke.github.PagedIterable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Component
@Transactional
public class CommitClassifier {

    @Autowired
    private CommitRepository commitRepository;

    @Autowired
    private GHRepository katanaRepo;

    @Autowired
    private BendingspoonsInternalCommiters bendingspoonsInternalCommiters;


    public void until(LocalDate date) throws IOException {
        PagedIterable<GHCommit> katanaCommits = katanaRepo.queryCommits()
                .since(Instant.now().minus(1,ChronoUnit.CENTURIES).toEpochMilli())
                .until(DateUtil.fromLocalDate(date).toEpochMilli()).list();
        for (GHCommit katanaCommit : katanaCommits) {
            Optional<GHUser> optkatanaCommitAuthor = ofNullable(katanaCommit.getAuthor());
            Commit commit = new Commit();
            commit.setSha(katanaCommit.getSHA1());
            commit.setDate(DateUtil.toLocalDate(katanaCommit.getCommitDate()));
            if(optkatanaCommitAuthor.isPresent()){
                commit.setAuthor(optkatanaCommitAuthor.get().getName());
                commit.setExternal(bendingspoonsInternalCommiters.isThisExternal(optkatanaCommitAuthor.get().getLogin()));
            }
            commit.setMessage(ofNullable(katanaCommit.getCommitShortInfo()).map(GHCommit.ShortInfo::getMessage).orElse(null));
            commitRepository.save(commit);
        }

    }



}
